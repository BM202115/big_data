from django.urls import path 
from . import views

urlpatterns = [
    path('', views.index, name='index'),    
    path('add-user/', views.add_user, name='add-user'),
    path('user-list/', views.get_users, name='user-list'),   
    path('add-youtube/', views.add_youtube, name='add-youtube'),    
    path('youtube-list/', views.get_youtube, name='youtube-list'), 
    path('add-tiktok/', views.add_tiktok, name='add-tiktok'),
    path('tiktok-list/', views.get_tiktok, name='tiktok-list'), 
    path('add-instagram/', views.add_instagram, name='add-instagram'), 
    path('instagram-list/', views.get_instagram, name='instagram-list'),   
    path('add-facebook/', views.add_facebook, name='add-facebook'), 
    path('facebook-list/', views.get_facebook, name='facebook-list'),   
    path('add-vkontakte/', views.add_vkontakte, name='add-vkontakte'), 
    path('vkontakte-list/', views.get_vkontakte, name='vkontakte-list'),   
    path('add-image/', views.add_image, name='add-image'), 
    path('image-list/', views.get_image, name='image-list'),   
    path('login/', views.login_page, name='login'),    
]