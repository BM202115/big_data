from django.shortcuts import render, redirect 
from . import models
from django.contrib.auth.models import User 
from django.contrib.auth import authenticate, login, logout 
from django.contrib import messages 
def index(request):
    return render(request, 'index.html')


def add_user(request):
    if request.method == 'POST':
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        local_address = request.POST.get('local_address')
        foreign_address = request.POST.get('foreign_address')
        gender = request.POST.get('gender')
        date_of_birth = request.POST.get('date_of_birth')
        img = request.FILES.get('img')
        models.CustomUser.objects.create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            local_address=local_address,
            foreign_address=foreign_address,
            gender=gender,
            date_of_birth=date_of_birth,
            img=img,
        )
        return redirect('user-list')
    return render(request, 'store/create_user.html')


def get_users(request):
    users = models.CustomUser.objects.all() 
    context = {
        'users': users,
    }
    return render(request, 'store/user_list.html', context)



def add_youtube(request):
    users = models.CustomUser.objects.all()
    context = {
        'users': users,
    }
    
    if request.method == 'POST':
        user = request.POST.get('user_id')
        channel = request.POST.get('channel')
        video = request.POST.get('video')
        url = request.POST.get('url')
        followers = request.POST.get('followers')
        views = request.POST.get('views')
        comment = request.POST.get('comment')
        country = request.POST.get('country')
        created_date = request.POST.get('created_date')
        social_media = 'YouTube'
        models.UserChannel.objects.create(
            user_id=user,
            channel=channel,
            video=video,
            url=url,
            followers=followers,
            views=views,
            comment=comment,
            country=country,
            created_date=created_date,
            social_media=social_media,
        )
        return redirect('youtube-list')
    return render(request, 'store/create_youtube.html', context)


def get_youtube(request):
    youtube = models.UserChannel.objects.filter(social_media='YouTube')
    context = {
        'youtube': youtube,
    }
    return render(request, 'store/youtube_list.html', context)

def add_tiktok(request):
    users = models.CustomUser.objects.all()
    context = {
        'users': users,
    }
    
    if request.method == 'POST':
        user = request.POST.get('user_id')
        channel = request.POST.get('channel')
        video = request.POST.get('video')
        url = request.POST.get('url')
        followers = request.POST.get('followers')
        views = request.POST.get('views')
        comment = request.POST.get('comment')
        country = request.POST.get('country')
        created_date = request.POST.get('created_date')
        social_media = 'TikTok'
        models.UserChannel.objects.create(
            user_id=user,
            channel=channel,
            video=video,
            url=url,
            followers=followers,
            views=views,
            comment=comment,
            country=country,
            created_date=created_date,
            social_media=social_media,
        )
        return redirect('tiktok-list')
    return render(request, 'store/create_tiktok.html', context)


def get_tiktok(request):
    tiktok = models.UserChannel.objects.filter(social_media='TikTok')
    context = {
        'tiktok': tiktok,
    }
    return render(request, 'store/tiktok_list.html', context)
    


## Instagramm kanalynyn funksiyasy
def add_instagram(request):
    users = models.CustomUser.objects.all()
    context = {
        'users': users,
    }
    
    if request.method == 'POST':
        user = request.POST.get('user_id')
        channel = request.POST.get('channel')
        video = request.POST.get('video')
        url = request.POST.get('url')
        followers = request.POST.get('followers')
        views = request.POST.get('views')
        comment = request.POST.get('comment')
        country = request.POST.get('country')
        created_date = request.POST.get('created_date')
        social_media = 'Instagram'
        models.UserChannel.objects.create(
            user_id=user,
            channel=channel,
            video=video,
            url=url,
            followers=followers,
            views=views,
            comment=comment,
            country=country,
            created_date=created_date,
            social_media=social_media,
        )
        return redirect('instagram-list')
    return render(request, 'store/create_instagram.html', context)


def get_instagram(request):
    instagram = models.UserChannel.objects.filter(social_media='Instagram')
    context = {
        'instagram': instagram,
    }
    return render(request, 'store/instagram_list.html', context)
    
# facebook kanalynyn funksiyasy
def add_facebook(request):
    users = models.CustomUser.objects.all()
    context = {
        'users': users,
    }
    
    if request.method == 'POST':
        user = request.POST.get('user_id')
        channel = request.POST.get('channel')
        video = request.POST.get('video')
        url = request.POST.get('url')
        followers = request.POST.get('followers')
        views = request.POST.get('views')
        comment = request.POST.get('comment')
        country = request.POST.get('country')
        created_date = request.POST.get('created_date')
        social_media = 'Facebook'
        models.UserChannel.objects.create(
            user_id=user,
            channel=channel,
            video=video,
            url=url,
            followers=followers,
            views=views,
            comment=comment,
            country=country,
            created_date=created_date,
            social_media=social_media,
        )
        return redirect('facebook-list')
    return render(request, 'store/create_facebook.html', context)


def get_facebook(request):
    facebook = models.UserChannel.objects.filter(social_media='Facebook')
    context = {
        'facebook': facebook,
    }
    return render(request, 'store/facebook_list.html', context)

# vkontakte kanalynyn funksiyasy
def add_vkontakte(request):
    users = models.CustomUser.objects.all()
    context = {
        'users': users,
    }
    
    if request.method == 'POST':
        user = request.POST.get('user_id')
        channel = request.POST.get('channel')
        video = request.POST.get('video')
        url = request.POST.get('url')
        followers = request.POST.get('followers')
        views = request.POST.get('views')
        comment = request.POST.get('comment')
        country = request.POST.get('country')
        created_date = request.POST.get('created_date')
        social_media = 'Vkontakte'
        models.UserChannel.objects.create(
            user_id=user,
            channel=channel,
            video=video,
            url=url,
            followers=followers,
            views=views,
            comment=comment,
            country=country,
            created_date=created_date,
            social_media=social_media,
        )
        return redirect('vkontakte-list')
    return render(request, 'store/create_vkontakte.html', context)


def get_vkontakte(request):
    vkontakte = models.UserChannel.objects.filter(social_media='Vkontakte')
    context = {
        'vkontakte': vkontakte,
    }
    return render(request, 'store/vkontakte_list.html', context)

# surat funksiyasy
def add_image(request):
    users = models.CustomUser.objects.all()
    context = {
        'users': users,
    }
    
    if request.method == 'POST':
        user = request.POST.get('user_id')
        social_media = 'Image'
        models.UserChannel.objects.create(
            user_id=user,
            social_media=social_media,
        )
        return redirect('image-list')
    return render(request, 'store/create_image.html', context)


def get_image(request):
    image = models.UserChannel.objects.filter(social_media='Image')
    context = {
        'image': image,
    }
    return render(request, 'store/image_list.html', context)






def login_page(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('index')
        else:
            messages.info(request, 'Username or password is incorrect')
    return render(request, 'users/login.html')