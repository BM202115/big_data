from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(models.Model):
    GENDER_TYPES = (
        ('male', 'Male'),
        ('female', 'Female')
    )
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField(max_length=100, blank=True, null=True)
    local_address = models.CharField(max_length=255, blank=True, null=True)
    foreign_address = models.CharField(max_length=255, blank=True, null=True)
    gender = models.CharField(max_length=6, choices=GENDER_TYPES, blank=True, null=True)
    date_of_birth = models.DateField(blank=True, null=True)
    img = models.ImageField(blank=True, null=True, upload_to='images/')
    
    class Meta:
        db_table = 'user'
        verbose_name = 'user'
        verbose_name_plural = 'users'
        
    def __str__(self):
        return f'{self.first_name} {self.last_name}'
    


class UserChannel(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    social_media = models.CharField(max_length=255, blank=True, null=True)
    channel = models.CharField(max_length=100)
    url = models.URLField(max_length=200)
    video = models.SmallIntegerField()
    followers = models.SmallIntegerField()
    views = models.SmallIntegerField()
    comment = models.SmallIntegerField()
    country = models.CharField(max_length=100)
    created_date = models.DateField()

    class Meta:
        db_table = 'user_channel'
    
    def __str__(self):
        return f'{self.user} ==> {self.channel}'